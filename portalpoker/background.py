# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from dbus_next import Variant

import click
import logging

from portalpoker import Request, PortalPoker

from typing import List

logger = logging.getLogger("Background")


@click.group(name="Background")
@click.pass_context
def background(ctx):
    pp = ctx.obj
    pp.portal = pp.load_portal("org.freedesktop.portal.Background")


@background.command(name="RequestBackground")
@click.option("--reason", type=str, default=None)
@click.option("--autostart", is_flag=True, default=False)
@click.option("--commandline", type=str, default="")
@click.option("--dbus-activatable", is_flag=True, default=False)
@click.pass_context
def request_background(
    ctx, reason: str, autostart: bool, commandline: str, dbus_activatable: bool
):
    pp = ctx.obj
    portal = pp.portal

    def result(result):
        if result.response == 0:
            print(", ".join([f"{k}: {v.value}" for k, v in result.results.items()]))
        print(result.response_string)
        pp.quit()

    options = {}
    if reason:
        options["reason"] = Variant("s", reason)
    if commandline:
        options["commandline"] = Variant("s", commandline.split(" "))  # good enough for this tool
    options["autostart"] = Variant("b", autostart)
    options["dbus-activatable"] = Variant("b", dbus_activatable)

    portal.request(
        "RequestBackground",
        callback=result,
        parent_window="",
        options=options,
    )
    pp.run()


# ############## impl.portal ############### #


@click.group(name="Background")
@click.pass_context
def impl_background(ctx):
    interface = "org.freedesktop.impl.portal.Background"
    if not ctx.obj:
        pp = PortalPoker.create_for_impl(interface)
        ctx.obj = pp
    pp.portal = pp.load_portal(interface)


@impl_background.command(name="GetAppState")
@click.pass_context
def impl_get_app_state(ctx):
    pp = ctx.obj
    portal = pp.portal
    states = portal.method("GetAppState")

    lut = {
        0: "background",
        1: "running",
        2: "active",
    }
    for appid, state in states.items():
        click.echo(f"{appid}: {lut[state.value]}")


@impl_background.command(name="NotifyBackground")
@click.argument("appid", type=str)
@click.argument("name", type=str)
@click.pass_context
def impl_notify_background(ctx, appid: str, name: str):
    pp = ctx.obj
    portal = pp.portal
    with Request.new_impl(portal.bus, portal.busname) as request:
        # response is unused
        _, results = portal.method(
            "NotifyBackground",
            callback=None,
            handle=request.handle,
            app_id=appid,
            name=name,
        )
    lut = {0: "forbidden", 1: "allowed", 2: "allowed this time"}
    click.echo(lut[results["result"].value])


@impl_background.command(name="EnableAutostart")
@click.option("--disable", is_flag=True)
@click.option("--use-dbus-activation", is_flag=True)
@click.argument("appid", type=str)
@click.argument("cmdline", type=str, nargs=-1)
@click.pass_context
def impl_enable_autostart(
    ctx, disable: bool, use_dbus_activation: bool, appid: str, cmdline: List[str]
):
    pp = ctx.obj
    portal = pp.portal
    flags = 1 if use_dbus_activation else 0

    # response is unused
    result = portal.method(
        "EnableAutostart",
        callback=None,
        app_id=appid,
        enable=not disable,
        commandline=list(cmdline),
        flags=flags,
    )
    click.echo(f"Autostart: {'enabled' if result else 'disabled'}")
