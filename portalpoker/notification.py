# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from dbus_next import Variant
from gi.repository import GLib

import click
import logging

logger = logging.getLogger("Notification")


@click.group(name="Notification")
@click.pass_context
def notification(ctx):
    pp = ctx.obj
    pp.portal = pp.load_portal("org.freedesktop.portal.Notification")


@notification.command(name="AddNotification")
@click.option("--title", type=str)
@click.argument("id", type=str)
@click.argument("body", type=str)
@click.pass_context
def add_notification(ctx, id: str, title: str, body: str):
    pp = ctx.obj
    portal = pp.portal

    def result(id, action, parameter):
        if id != id:
            return
        click.echo(f"Signal for {id}: {action}, {parameter}")
        pp.quit()

    portal.signal("ActionInvoked", result)

    notification = {"body": Variant("s", body)}
    if title:
        notification["title"] = Variant("s", title)

    portal.method(
        "AddNotification",
        id=id,
        notification=notification,
    )
    click.echo("Notification requested, waiting")
    GLib.timeout_add(2000, pp.quit)
    pp.run()


@notification.command(name="RemoveNotification")
@click.argument("id", type=str)
@click.pass_context
def remove_notification(ctx, id: str):
    pp = ctx.obj
    portal = pp.portal

    portal.method(
        "RemoveNotification",
        id=id,
    )


# ############## impl.portal ############### #


@click.group(name="Notification")
@click.pass_context
def impl_notification(ctx):
    interface = "org.freedesktop.impl.portal.Notification"
    if not ctx.obj:
        pp = PortalPoker.create_for_impl(interface)
        ctx.obj = pp
    pp.portal = pp.load_portal(interface)


@impl_notification.command(name="AddNotification")
@click.option("--title", type=str)
@click.argument("appid", type=str)
@click.argument("id", type=str)
@click.argument("body", type=str)
@click.pass_context
def impl_add_notification(ctx, appid: str, id: str, title: str, body: str):
    pp = ctx.obj
    portal = pp.portal

    notification = {"body": Variant("s", body)}
    if title:
        notification["title"] = Variant("s", title)

    portal.method(
        "AddNotification",
        app_id=appid,
        id=id,
        notification=notification,
    )


@impl_notification.command(name="RemoveNotification")
@click.argument("appid", type=str)
@click.argument("id", type=str)
@click.pass_context
def impl_remove_notification(ctx, appid: str, id: str):
    pp = ctx.obj
    portal = pp.portal

    portal.method(
        "RemoveNotification",
        app_id=appid,
        id=id,
    )
