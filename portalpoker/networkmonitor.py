#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from dbus_next import Variant
from pathlib import Path

import click
import logging

logger = logging.getLogger("NetworkMonitor")

connectivity_lut = {
    1: "local only",
    2: "limited connectivity",
    3: "captive portal",
    4: "full network",
}


def yesno(b: bool):
    return "yes" if b else "no"


@click.group(name="NetworkMonitor")
@click.pass_context
def networkmonitor(ctx):
    pp = ctx.obj
    pp.portal = pp.load_portal("org.freedesktop.portal.NetworkMonitor")


@networkmonitor.command(name="GetAvailable")
@click.pass_context
def get_available(ctx):
    pp = ctx.obj
    portal = pp.portal

    click.echo(yesno(portal.method("GetAvailable")))


@networkmonitor.command(name="GetMetered")
@click.pass_context
def get_metered(ctx):
    pp = ctx.obj
    portal = pp.portal

    click.echo(yesno(portal.method("GetMetered")))


@networkmonitor.command(name="GetConnectivity")
@click.pass_context
def get_connectivity(ctx):
    pp = ctx.obj
    portal = pp.portal

    conn = portal.method("GetConnectivity")
    click.echo(connectivity_lut[conn])


@networkmonitor.command(name="GetStatus")
@click.pass_context
def get_status(ctx):
    pp = ctx.obj
    portal = pp.portal

    results = portal.method("GetStatus")

    click.echo(f"available: {yesno(results['available'])}")
    click.echo(f"metered: {yesno(results['metered'])}")
    click.echo(f"connectivity: {connectivity_lut[results['available'].value]}")


@networkmonitor.command(name="CanReach")
@click.argument("hostname", type=str)
@click.argument("port", type=int)
@click.pass_context
def can_reach(ctx, hostname, port):
    pp = ctx.obj
    portal = pp.portal

    click.echo(portal.method("CanReach", hostname=hostname, port=port))
