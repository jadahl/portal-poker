# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from dbus_next import Variant
from typing import Callable, List

import click
import logging

from portalpoker import Session, Portal, Request, PortalPoker

logger = logging.getLogger("RemoteDesktop")


def mkt_to_mask(mkt: str):
    mask = 0
    if "k" in mkt:
        mask |= 0x1
    if "m" in mkt:
        mask |= 0x2
    if "t" in mkt:
        mask |= 0x4
    return mask


def create_session(portal: Portal, callback: Callable):
    session = Session.new(portal.bus, portal.busname)

    def session_created(response):
        assert response.response == 0
        assert response.results["session_handle"].value == session.handle
        logger.debug(f"Session handle: {session.handle}")
        callback(response)

    options = session.add_token_to_options({})
    portal.request(
        "CreateSession",
        callback=session_created,
        options=options,
    )

    return session


@click.group(name="RemoteDesktop")
@click.option(
    "--keepalive", is_flag=True, help="Don't quit after the DBus interaction is finshed"
)
@click.pass_context
def remotedesktop(ctx, keepalive):
    pp = ctx.obj
    pp.portal = pp.load_portal("org.freedesktop.portal.RemoteDesktop")
    pp.keepalive = keepalive


@remotedesktop.command(name="AvailableDeviceTypes")
@click.pass_context
def available_device_types(ctx):
    pp = ctx.obj
    portal = pp.portal

    types = []
    mask = portal.property("AvailableDeviceTypes")
    if mask & 0x1:
        types += ["keyboard"]
    if mask & 0x2:
        types += ["pointer"]
    if mask & 0x4:
        types += ["touchscreen"]

    click.echo(f"Available types: {', '.join(types)}")


@remotedesktop.command(name="SelectDevices")
@click.argument("types", type=str)
@click.pass_context
def cli_select_devices(ctx, types):
    """
    Select one or more of "mkt" for mouse, keyboard, touch
    """
    pp = ctx.obj

    def session_created(results):
        mask = mkt_to_mask(types)
        options = {
            "types": Variant("u", mask),
        }
        pp.portal.request(
            "SelectDevices",
            callback=None,
            session_handle=pp.session.handle,
            options=options,
        )
        if not pp.keepalive:
            pp.session.close()
            pp.quit()

    pp.session = create_session(pp.portal, session_created)
    pp.run()


@remotedesktop.command(name="Start")
@click.option("--types", type=str, default="mkt")
@click.pass_context
def cli_start(ctx, types):
    pp = ctx.obj

    def start_done(results):
        click.echo(
            f"Result: {results.response} with mask {results.results['devices'].value:0x}"
        )
        if not pp.keepalive:
            pp.session.close()
            pp.quit()

    def select_devices_done(results):
        assert results.response == 0
        pp.portal.request(
            "Start",
            callback=start_done,
            session_handle=pp.session.handle,
            parent_window="",
        )

    def session_created(results):
        mask = mkt_to_mask(types)
        options = {
            "types": Variant("u", mask),
        }
        pp.portal.request(
            "SelectDevices",
            callback=select_devices_done,
            session_handle=pp.session.handle,
            options=options,
        )

    pp.session = create_session(pp.portal, session_created)
    pp.run()


def notify(
    portal: Portal, types: str, event_callback: Callable[[Portal, Session], None]
):
    session = None

    def start_done(results):
        assert results.response == 0
        event_callback(portal, session)

    def select_devices_done(results):
        assert results.response == 0
        portal.request(
            "Start",
            callback=start_done,
            session_handle=session.handle,
            parent_window="",
        )

    def session_created(results):
        mask = mkt_to_mask(types)
        options = {
            "types": Variant("u", mask),
        }
        portal.request(
            "SelectDevices",
            callback=select_devices_done,
            session_handle=session.handle,
            options=options,
        )

    session = create_session(portal, session_created)


@remotedesktop.command(name="NotifyPointerMotion")
@click.option("--types", type=str, default="mkt")
@click.argument("events", nargs=-1)
@click.pass_context
def cli_notify_pointer_motion(ctx, types, events):
    """
    Send the given events through this session. Events are a sequence of
    strings in the form dx,dy.
    """
    pp = ctx.obj
    portal = pp.portal

    def send_events(portal, session):
        for v in events:
            x, y = [float(d) for d in v.split(",")]
            portal.method(
                "NotifyPointerMotion",
                session_handle=session.handle,
                options={},
                dx=x,
                dy=y,
            )
        if not pp.keepalive:
            session.close()
            pp.quit()

    notify(portal, types, send_events)
    pp.run()


@remotedesktop.command(name="Notify")
@click.option("--types", type=str, default="mkt")
@click.argument("events", nargs=-1)
@click.pass_context
def cli_notify(ctx, types, events):
    """
    A generic version of Notify* that can take multiple different arguments.

    Send the given events through this session. Events are a sequence of
    strings in the form type:values where the values are usually
    comma-separated depending on the type, e.g.

        rel:1,0 rel:-5,-10
    """
    pp = ctx.obj
    portal = pp.portal

    def send_events(portal, session):
        for type, values in [x.split(":") for x in events]:
            if type == "rel":
                x, y = [float(d) for d in values.split(",")]

                def cb(result):
                    pass

                portal.method(
                    "NotifyPointerMotion",
                    session_handle=session.handle,
                    callback=cb,
                    options={},
                    dx=x,
                    dy=y,
                )
            # FIXME: the others, if needed
        if not pp.keepalive:
            pp.quit()

    notify(portal, types, send_events)
    pp.run()


# ############## impl.portal ############### #


@click.group(name="RemoteDesktop")
@click.option(
    "--keepalive", is_flag=True, help="Don't quit after the DBus interaction is finshed"
)
@click.pass_context
def impl_remotedesktop(ctx, keepalive):
    interface = "org.freedesktop.impl.portal.RemoteDesktop"
    if not ctx.obj:
        pp = PortalPoker.create_for_impl(interface)
        ctx.obj = pp
    pp = ctx.obj
    pp.portal = pp.load_portal(interface)
    pp.keepalive = keepalive


@impl_remotedesktop.command(name="AvailableDeviceTypes")
@click.pass_context
def impl_available_device_types(ctx):
    pp = ctx.obj
    portal = pp.portal

    types = []
    mask = portal.property("AvailableDeviceTypes")
    if mask & 0x1:
        types += ["keyboard"]
    if mask & 0x2:
        types += ["pointer"]
    if mask & 0x4:
        types += ["touchscreen"]

    click.echo(f"Available types: {', '.join(types)}")


def impl_create_session(portal: Portal, appid: str):
    session = Session.new_impl(portal.bus, portal.busname)

    with Request.new_impl(portal.bus, portal.busname) as request:
        response, results = portal.method(
            "CreateSession",
            handle=request.handle,
            session_handle=session.handle,
            app_id=appid,
            options={},
        )
        if response != 0:
            logger.error("Failed to create session: {response} {results}")
            return None

    return session


@impl_remotedesktop.command(name="SelectDevices")
@click.argument("appid", type=str)
@click.argument("types", type=str)
@click.pass_context
def impl_select_devices(ctx, appid: str, types: str):
    pp = ctx.obj
    portal = pp.portal

    with impl_create_session(portal, appid) as session:
        with Request.new_impl(portal.bus, portal.busname) as request:
            mask = mkt_to_mask(types)
            options = {
                "types": Variant("u", mask),
            }
            response, results = portal.method(
                "SelectDevices",
                handle=request.handle,
                session_handle=session.handle,
                app_id=appid,
                options=options,
            )
            # FIXME: this appears to always return 0


@impl_remotedesktop.command(name="Start")
@click.option("--types", type=str, default="mkt")
@click.argument("appid", type=str)
@click.pass_context
def impl_start(ctx, types: str, appid: str):
    pp = ctx.obj
    portal = pp.portal

    with impl_create_session(portal, appid) as session:
        with Request.new_impl(portal.bus, portal.busname) as request:
            mask = mkt_to_mask(types)
            options = {
                "types": Variant("u", mask),
            }
            response, results = portal.method(
                "SelectDevices",
                handle=request.handle,
                session_handle=session.handle,
                app_id=appid,
                options=options,
            )
            # FIXME: this appears to always return 0

        with Request.new_impl(portal.bus, portal.busname) as request:
            response, results = portal.method(
                "Start",
                handle=request.handle,
                session_handle=session.handle,
                app_id=appid,
                parent_window="",
                options=options,
            )

            if response != 0:
                click.secho("Start() failed", fg="red")
            else:
                mask = results["devices"].value
                allowed_types = []
                if mask & 0x1:
                    allowed_types += ["keyboard"]
                if mask & 0x2:
                    allowed_types += ["pointer"]
                if mask & 0x4:
                    allowed_types += ["touchscreen"]
                click.echo(f"Available types: {', '.join(allowed_types)}")


@impl_remotedesktop.command(name="NotifyPointerMotion")
@click.option("--types", type=str, default="mkt")
@click.argument("appid", type=str)
@click.argument("events", nargs=-1)
@click.pass_context
def impl_notify_pointer_motion(ctx, types: str, appid: str, events: List[str]):
    pp = ctx.obj
    portal = pp.portal

    with impl_create_session(portal, appid) as session:
        with Request.new_impl(portal.bus, portal.busname) as request:
            mask = mkt_to_mask(types)
            options = {
                "types": Variant("u", mask),
            }
            response, results = portal.method(
                "SelectDevices",
                handle=request.handle,
                session_handle=session.handle,
                app_id=appid,
                options=options,
            )
            # FIXME: this appears to always return 0

        with Request.new_impl(portal.bus, portal.busname) as request:
            response, results = portal.method(
                "Start",
                handle=request.handle,
                session_handle=session.handle,
                app_id=appid,
                parent_window="",
                options=options,
            )
            assert response == 0

        for v in events:
            x, y = [float(d) for d in v.split(",")]
            portal.method(
                "NotifyPointerMotion",
                session_handle=session.handle,
                options={},
                dx=x,
                dy=y,
            )
