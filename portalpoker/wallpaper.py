# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from dbus_next import Variant
from pathlib import Path

from portalpoker import Request, PortalPoker

import click
import logging

logger = logging.getLogger("Wallpaper")


@click.group(name="Wallpaper")
@click.pass_context
def wallpaper(ctx):
    pp = ctx.obj
    pp.portal = pp.load_portal("org.freedesktop.portal.Wallpaper")


@wallpaper.command(name="SetWallpaperURI")
@click.argument("uri", type=str)
@click.option("--preview/--no-preview", default=True)
@click.option(
    "--set-on",
    type=click.Choice(["background", "lockscreen", "both"]),
    default="background",
)
@click.pass_context
def set_wallpaper_uri(ctx, uri: str, preview: bool, set_on: str):
    pp = ctx.obj
    portal = pp.portal

    def result(result):
        print(result.response_string)
        pp.quit()

    portal.request(
        "SetWallpaperURI",
        callback=result,
        parent_window="",
        uri=uri,
        options={
            "set-on": Variant("s", set_on),
            "show-preview": Variant("b", preview),
        },
    )
    pp.run()


@wallpaper.command(name="SetWallpaperFile")
@click.argument("filename", type=Path)
@click.pass_context
def set_wallpaper_file(ctx, filename: Path):
    raise NotImplementedError(
        "dbus_next does not yet support fd passing, see "
        "https://github.com/altdesktop/python-dbus-next/issues/33",
    )


# ############## impl.portal ############### #


@click.group(name="Wallpaper")
@click.pass_context
def impl_wallpaper(ctx):
    interface = "org.freedesktop.impl.portal.Wallpaper"
    if not ctx.obj:
        pp = PortalPoker.create_for_impl(interface)
        ctx.obj = pp
    pp.portal = pp.load_portal(interface)


@impl_wallpaper.command(name="SetWallpaperURI")
@click.argument("appid", type=str)
@click.argument("uri", type=str)
@click.option("--preview/--no-preview", default=True)
@click.option(
    "--set-on",
    type=click.Choice(["background", "lockscreen", "both"]),
    default="background",
)
@click.pass_context
def impl_set_wallpaper_uri(ctx, appid: str, uri: str, preview: bool, set_on: str):
    pp = ctx.obj
    portal = pp.portal

    with Request.new_impl(portal.bus, portal.busname) as request:
        portal.method(
            "SetWallpaperURI",
            callback=None,
            handle=request.handle,
            app_id=appid,
            parent_window="x",
            uri=uri,
            options={
                "set-on": Variant("s", set_on),
                "show-preview": Variant("b", preview),
            },
        )
