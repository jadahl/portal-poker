#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from dbus_next.glib import MessageBus
from dbus_next import BusType, DBusError, Variant
from pathlib import Path
from gi.repository import GLib
from typing import Any, Callable, Dict, List, Optional
from itertools import count

import attr
import logging
import re
import xml.etree.ElementTree as ET


ResponseCallback = Callable[["Response"], None]
MethodCallback = Callable[[List[Any]], None]

logger = logging.getLogger("portal-poker.core")

basedir = Path("/usr/share/dbus-1/interfaces")

_CACHED_INTROSPECTION_FILES: Dict[str, str] = {}


def load_introspection_xml(filename: str):
    try:
        introspection = _CACHED_INTROSPECTION_FILES[filename]
    except KeyError:
        with open(basedir / filename) as f:
            introspection = f.read()
        _CACHED_INTROSPECTION_FILES[filename] = introspection
    return introspection


class PortalError(Exception):
    pass


@attr.s
class HandleToken:
    _counter = count()
    token: str = attr.ib()

    @classmethod
    def create(cls):
        return cls(token=f"token{next(cls._counter)}")


@attr.s
class SenderToken:
    token: str = attr.ib()

    @classmethod
    def from_bus(cls, bus: MessageBus):
        assert bus.unique_name is not None
        return cls.from_sender_string(bus.unique_name)

    @classmethod
    def from_sender_string(cls, sender: str):
        sender_token = sender.removeprefix(":").replace(".", "_")
        return cls(token=sender_token)


@attr.s
class Portal:
    _CACHED_FILES: Dict[str, str] = {}
    intf = attr.ib()
    proxy = attr.ib()

    @property
    def bus(self):
        return self.proxy.bus

    @property
    def busname(self) -> str:
        return self.proxy.bus_name

    @property
    def name(self) -> str:
        return self.intf.introspection.name

    @property
    def version(self) -> int:
        return self.intf.get_version_sync()

    @classmethod
    def _dbus_name(cls, name: str) -> str:
        """CamelCase to snake_case"""
        return re.sub(r"([A-Z]+)", r"_\1", name).lower()

    def property(self, name: str) -> Any:
        """
        Return the value of the property with the given name
        """
        dbus_name = self._dbus_name(name)
        prop = getattr(self.intf, f"get{dbus_name}_sync")
        return prop()

    def method(
        self, method_name: str, callback: Optional[MethodCallback] = None, **kwargs
    ) -> Any:
        """
        Call the given method name with the kwargs (in-order) as arguments and
        return its values, if any.

        If callback is None, the method is run as sync version, otherwise the
        callback is invoked with the results of the call.
        """
        dbus_name = self._dbus_name(method_name)
        args = [v for k, v in kwargs.items()]
        logger.debug(f"Calling {method_name} with args: {args}")
        if callback:

            def method_async_callback(result: List[Any], error: Exception):
                logger.debug(f"-> Result for {method_name}: {result}")
                assert callback
                callback(result)

            method = getattr(self.intf, f"call{dbus_name}")
            method(*args, method_async_callback)
            return None
        else:
            method = getattr(self.intf, f"call{dbus_name}_sync")
            results = method(*args)
            logger.debug(f"-> Result: {results}")
            return results

    def signal(self, name: str, callback: Callable) -> None:
        """
        Hook up the callback to the signal with the given name.
        """
        dbus_name = self._dbus_name(name)
        signal = getattr(self.intf, f"on{dbus_name}")
        signal(callback)

    def request(
        self, name: str, callback: Optional[ResponseCallback], **kwargs
    ) -> "Request":
        """
        Call the given method name with the kwargs (in-order) as arguments.

        If a callback is given it is invoked with the Result of the Response.
        """
        dbus_name = self._dbus_name(name)
        method = getattr(self.intf, f"call{dbus_name}")

        def result_callback(result):
            if result.response != result.SUCCESS:
                logger.error(f"Request failed with: {result.response}")

            if callback is not None:
                callback(result)

        r = Request.new_with_callback(self.bus, self.busname, result_callback)
        try:
            options = kwargs["options"]
        except KeyError:
            options = {}
            kwargs["options"] = options
        options["handle_token"] = Variant("s", r.token)

        args = [v for k, v in kwargs.items() if k != "callback"]
        logger.debug(f"Requesting {name} with args: {args}")

        def request_async_callback(result: List[Any], error: Exception):
            if error is None:
                objpath = result[0]
                assert objpath == r.handle

        method(*args, request_async_callback)

        logger.debug(f"Request is at {r.handle}")

        return r

    def __str__(self) -> str:
        str = [f"{self.name}:", "  Properties:", f"    version = {self.version}"]

        for p in self.intf.introspection.properties:
            if p.name != "version":
                str += [f"    {p.name} = {p.signature}"]

        if self.intf.introspection.methods:
            str += ["  Methods: "]

        for m in self.intf.introspection.methods:
            in_sig = ", ".join([f"{a.name}: {a.signature}" for a in m.in_args])
            if m.out_args:
                out_sig = ", ".join([f"{a.name}: {a.signature}" for a in m.out_args])
            else:
                out_sig = "None"
            m_sig = f"    {m.name}({in_sig}) -> {out_sig}"
            str += [m_sig]

        if self.intf.introspection.signals:
            str += ["  Signals: "]

        for s in self.intf.introspection.signals:
            str += [f"    {s.name}: {s.signature}"]

        return "\n".join(str)

    @classmethod
    def from_file(
        cls, bus: MessageBus, busname: str, objpath: str, file: str
    ) -> "Portal":
        try:
            introspection = load_introspection_xml(file)
        except FileNotFoundError as e:
            raise PortalError(str(e))

        root = ET.fromstring(introspection)
        iface = root.find("interface")
        if not iface:
            raise PortalError(f"Invalid XML file {file}")
        iname = iface.get("name")
        if not iname:
            raise PortalError(f"Invalid XML file {file}")

        logger.debug(f"Loading interface: {iname}")

        proxy = bus.get_proxy_object(busname, objpath, introspection)  # type: ignore
        interface = proxy.get_interface(iname)

        return cls(intf=interface, proxy=proxy)

    @classmethod
    def from_portal_name(cls, bus: MessageBus, busname: str, name: str) -> "Portal":
        objpath = "/org/freedesktop/portal/desktop"

        return cls.from_file(bus, busname, objpath, f"{name}.xml")


@attr.s
class Response:
    """
    The response returned for a Request, if any
    """

    SUCCESS = 0
    CANCELLED = 1
    ENDED = 2

    response: int = attr.ib()
    results: Dict[str, Any] = attr.ib()

    @property
    def response_string(self) -> str:
        if self.response == 0:
            return "success"
        elif self.response == 1:
            return "cancelled"
        elif self.response == 2:
            return "error"


@attr.s
class Request:
    _counter = count()
    id: int = attr.ib(
        init=False,
        default=attr.Factory(lambda self: next(self._counter), takes_self=True),
    )
    token: str = attr.ib()
    handle: str = attr.ib()
    _portal: Portal = attr.ib()
    _is_closed: bool = attr.ib(init=False, default=False)

    def close(self) -> None:
        if self._is_closed:
            return

        self._is_closed = True

        def noop(result, error):
            pass

        logger.debug(f"Closing request {self.id}")
        self._portal.intf.call_close(noop)

    def __enter__(self) -> "Request":
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        self.close()

    @classmethod
    def new_with_callback(
        cls, bus: MessageBus, busname: str, callback: Callable
    ) -> "Request":
        sender_token = SenderToken.from_bus(bus).token
        handle_token = HandleToken.create().token
        request_objpath = (
            f"/org/freedesktop/portal/desktop/request/{sender_token}/{handle_token}"
        )

        request = Portal.from_file(
            bus=bus,
            busname=busname,
            objpath=request_objpath,
            file="org.freedesktop.portal.Request.xml",
        )

        r = Request(token=handle_token, handle=request_objpath, portal=request)

        def set_results(response, results):
            logger.debug(f"Response for request {r.id}: {response} {results}")
            callback(Response(response, results))

            r.close()

        request.intf.on_response(set_results)
        return r

    @classmethod
    def new_impl(cls, bus: MessageBus, busname: str) -> "Request":
        sender_token = SenderToken.from_bus(bus).token
        handle_token = HandleToken.create().token
        request_objpath = (
            f"/org/freedesktop/portal/desktop/request/{sender_token}/{handle_token}"
        )

        request = Portal.from_file(
            bus=bus,
            busname=busname,
            objpath=request_objpath,
            file="org.freedesktop.impl.portal.Request.xml",
        )

        return Request(token=handle_token, handle=request_objpath, portal=request)


@attr.s
class Session:
    _counter = count()
    id: int = attr.ib(
        init=False,
        default=attr.Factory(lambda self: next(self._counter), takes_self=True),
    )
    token: str = attr.ib()
    handle: str = attr.ib()
    _portal = attr.ib()
    _is_closed: bool = attr.ib(init=False, default=False)

    @property
    def session_handle(self) -> str:
        return self.handle

    def close(self) -> None:
        if self._is_closed:
            return

        self._is_closed = True

        def noop(result, error):
            pass

        logger.debug(f"Closing session {self.id}")
        self._portal.intf.call_close(noop)

    def __enter__(self) -> "Session":
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        self.close()

    def add_token_to_options(
        self, options: Optional[Dict[str, Variant]]
    ) -> Dict[str, Variant]:
        if not options:
            options = {}
        options["session_handle_token"] = Variant("s", self.token)
        return options

    @classmethod
    def new(cls, bus: MessageBus, busname: str) -> "Session":
        return cls.new_with_callback(bus, busname)

    @classmethod
    def new_with_callback(
        cls, bus: MessageBus, busname: str, closed_callback: Optional[Callable] = None
    ) -> "Session":
        sender_token = SenderToken.from_bus(bus).token
        handle_token = HandleToken.create().token
        session_objpath = (
            f"/org/freedesktop/portal/desktop/session/{sender_token}/{handle_token}"
        )

        session = Portal.from_file(
            bus=bus,
            busname=busname,
            objpath=session_objpath,
            file="org.freedesktop.portal.Session.xml",
        )

        s = Session(token=handle_token, handle=session_objpath, portal=session)

        def session_closed_callback(error):
            if closed_callback:
                closed_callback()
            s.close()

        session.intf.on_closed(session_closed_callback)
        return s

    @classmethod
    def new_impl(cls, bus: MessageBus, busname: str) -> "Session":
        sender_token = SenderToken.from_bus(bus).token
        handle_token = HandleToken.create().token
        session_objpath = (
            f"/org/freedesktop/portal/desktop/session/{sender_token}/{handle_token}"
        )

        session = Portal.from_file(
            bus=bus,
            busname=busname,
            objpath=session_objpath,
            file="org.freedesktop.impl.portal.Session.xml",
        )

        return Session(token=handle_token, handle=session_objpath, portal=session)


@attr.s
class PortalPoker:
    bus: MessageBus = attr.ib()
    busname: str = attr.ib()

    def run(self) -> None:
        try:
            self.mainloop = GLib.MainLoop()
            self.mainloop.run()
        except KeyboardInterrupt:
            pass

    def quit(self) -> None:
        GLib.idle_add(self.mainloop.quit)

    def show_portal_info(self, name: str) -> str:
        return str(Portal.from_portal_name(self.bus, self.busname, name))

    def load_portal(self, name: str) -> Portal:
        return Portal.from_portal_name(bus=self.bus, busname=self.busname, name=name)

    @classmethod
    def create(cls, busname: str) -> "PortalPoker":
        try:
            bus = MessageBus(bus_type=BusType.SESSION).connect_sync()
            logger.debug(f"Connected to DBus {busname}, we are {bus.unique_name}")
        except DBusError as e:
            raise PortalError(f"Failed to connect to DBus: {e}")

        return cls(bus=bus, busname=busname)

    @classmethod
    def create_for_impl(cls, interface: str) -> "PortalPoker":
        import os
        from configparser import ConfigParser

        desktop_type = os.getenv("XDG_SESSION_DESKTOP", "gnome")
        for portal_file in Path("/usr/share/xdg-desktop-portal/portals").glob(
            "*.portal"
        ):
            config = ConfigParser()
            config.read(portal_file)
            try:
                if config["portal"]["UseIn"] != desktop_type:
                    continue
                interfaces = config["portal"]["Interfaces"].split(";")
                if interface in interfaces:
                    logger.debug(f"Found requested interface in {portal_file}")
                    busname = config["portal"]["DBusName"]
                    return cls.create(busname)
            except KeyError:
                pass

        raise PortalError(f"Unable to find bus name for {interface}")
