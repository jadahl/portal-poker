Portal Poker
============

`portal-poker` is a commandline utility to poke the DBus API(s) of XDG desktop
portals.

As a rule of thumb, the CLI looks like one of these two, depending whether you
want to talk to the portal or the impl of the portal:

```
$ portal-poker portal PortalName MethodName arg1 arg2
$ portal-poker impl PortalName MethodName arg1 arg2
```

For example:
```
$ portal-poker portal RemoteDesktop Start
$ portal-poker portal Wallpaper SetWallpaperURI file://home/$USER/Pictures/DesktopBackground.png
$ portal-poker portal Notification AddNotification --title "title" "notification-id" "body content"

$ portal-poker impl RemoteDesktop Start
$ portal-poker impl Wallpaper SetWallpaperURI org.example.AppId file://home/$USER/Pictures/DesktopBackground.png
$ portal-poker impl Notification AddNotification --title "title" org.example.AppId "notification-id" "body content"
```
